<?php
	$year = date("Y");

	if (isset($_GET['year'])) {
		$year = $_GET['year'];
	}
	
	function makeCal($month, $year) {
		$daysInMonth = date('t',mktime(0,0,0,$month,1,$year));
		$startDayOfWeek = date('w',mktime(0,0,0,$month,1,$year));
		$monthName = date('F', mktime(0, 0, 0, $month, 10));
		$dayCount = 0;

		$cal = "\t\t".'<section class="sheet padding-5mm">'."\n";
		$cal .= "\t\t\t".'<h1>'.$monthName.' '.$year.'</h1>'."\n";
		$cal .= "\t\t\t".'<table>'."\n";
		$cal .= "\t\t\t\t".'<tr class="row-head">'."\n";
		$cal .= "\t\t\t\t\t".'<th class="day-head">Sun</th>'."\n";
		$cal .= "\t\t\t\t\t".'<th class="day-head">Mon</th>'."\n";
		$cal .= "\t\t\t\t\t".'<th class="day-head">Tues</th>'."\n";
		$cal .= "\t\t\t\t\t".'<th class="day-head">Wed</th>'."\n";
		$cal .= "\t\t\t\t\t".'<th class="day-head">Thur</th>'."\n";
		$cal .= "\t\t\t\t\t".'<th class="day-head">Fri</th>'."\n";
		$cal .= "\t\t\t\t\t".'<th class="day-head">Sat</th>'."\n";
		$cal .= "\t\t\t\t".'</tr>'."\n";
		$cal .= "\t\t\t\t".'<tr class="row">'."\n";

		while($dayCount != $startDayOfWeek) {
			$cal .= "\t\t\t\t\t".'<td class="day-np"></td>'."\n";
			$dayCount++;
		}
		
		for($day = 1; $day <= $daysInMonth; $day++) {
			$cal .= "\t\t\t\t\t".'<td class="day">';
			$cal .= $day;
			$cal.= '</td>'."\n";
			
			$dayCount++;
			if($dayCount % 7 == 0 && $dayCount <= $daysInMonth + $startDayOfWeek - 1) {
				$cal .= "\t\t\t\t".'</tr>'."\n";
				$cal .= "\t\t\t\t".'<tr class="row">'."\n";
			}
		}
		
		while($dayCount % 7 != 0) {
			$cal .= "\t\t\t\t\t".'<td class="day-np"></td>'."\n";
			$dayCount++;
		}

		$cal .= "\t\t\t\t".'</tr>'."\n";
		$cal .= "\t\t\t".'</table>'."\n";
		$cal .= "\t\t".'</section>'."\n";
		
		return $cal;
	}
	
	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Calendar Creator</title>
		<style>
			html {
				font-family: Helvetica;
				text-align: center;
			}
			
			h1 {
				margin: 0;
				padding: 0;
				padding-bottom: 10px;
			}
			
			table, th, td {
				border: 1.5px solid black;
				border-collapse: collapse;
			}
			
			table {
				table-layout: fixed;
				border: 4px solid black;
				width: 100%;
				height: 95%;
			}
			
			.day-head {
				background: #2460c1;
				font-size: 20px;
				height: 50px;
				border-bottom: 4px solid black;
				color: white;
				vertical-align: middle;
			}
			
			.day-np {
				background: #888;
			}
			
			.day {
				padding-top: 15px;
				padding-left: 15px;
				font-weight: bold;
				vertical-align: top;
				text-align: left;
			}
			
			.day:first-child { 
				background-color: #ccc;
			}
			
			.day:last-child { 
				background-color: #ccc;
			}
			
			@page {
				size: letter landscape;
				margin: 0;
			}
			
			body {
				margin: 0;
			}
			
			.sheet {
				margin: 0;
				overflow: hidden;
				position: relative;
				box-sizing: border-box;
				page-break-after: always;
			}

			/** Paper sizes **/
			body.A3               .sheet { width: 297mm; height: 419mm }
			body.A3.landscape     .sheet { width: 420mm; height: 296mm }
			body.A4               .sheet { width: 210mm; height: 296mm }
			body.A4.landscape     .sheet { width: 297mm; height: 209mm }
			body.A5               .sheet { width: 148mm; height: 209mm }
			body.A5.landscape     .sheet { width: 210mm; height: 147mm }
			body.letter           .sheet { width: 216mm; height: 279mm }
			body.letter.landscape .sheet { width: 280mm; height: 215mm }
			body.legal            .sheet { width: 216mm; height: 356mm }
			body.legal.landscape  .sheet { width: 357mm; height: 215mm }

			/** Padding area **/
			.sheet.padding-1mm { padding: 1mm }
			.sheet.padding-5mm { padding: 5mm }
			.sheet.padding-10mm { padding: 10mm }
			.sheet.padding-15mm { padding: 15mm }
			.sheet.padding-20mm { padding: 20mm }
			.sheet.padding-25mm { padding: 25mm }

			/** Fix for Chrome issue #273306 **/
			@media print {
				body.A3.landscape { width: 420mm }
				body.A3, body.A4.landscape { width: 297mm }
				body.A4, body.A5.landscape { width: 210mm }
				body.A5                    { width: 148mm }
				body.letter, body.legal    { width: 216mm }
				body.letter.landscape      { width: 280mm }
				body.legal.landscape       { width: 357mm }
			}
			
			/** For screen preview **/
			@media screen {
				body {
					background: #e0e0e0
				}
				
				.sheet {
					background: white;
					box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
					margin: 5mm auto;
				}
			}
		</style>
	</head>
	<body class="letter landscape">
<?php
		for ($m = 1; $m <= 12; $m++) {
			echo makeCal($m, $year);
		}
?>
	</body>
</html>